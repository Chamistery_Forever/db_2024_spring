-- Заполнение таблицы "movies"
INSERT INTO marvel_movies_database.movies (movie_id, movie_name, year) VALUES
(1, 'Iron Man', 2008),
(2, 'The Incredible Hulk', 2008),
(3, 'Iron Man 2', 2010),
(4, 'Thor', 2011),
(5, 'Captain America: The First Avenger', 2011),
(6, 'The Avengers', 2012),
(7, 'Iron Man 3', 2013),
(8, 'Thor: The Dark World', 2013),
(9, 'Captain America: The Winter Soldier', 2014),
(10, 'Guardians of the Galaxy', 2014),
(11, 'Avengers: Age of Ultron', 2015),
(12, 'Ant-Man', 2015),
(13, 'Captain America: Civil War', 2016),
(14, 'Doctor Strange', 2016),
(15, 'Guardians of the Galaxy Vol. 2', 2017),
(16, 'Spider-Man: Homecoming', 2017),
(17, 'Thor: Ragnarok', 2017),
(18, 'Black Panther', 2018),
(19, 'Avengers: Infinity War', 2018),
(20, 'Ant-Man and the Wasp', 2018),
(21, 'Captain Marvel', 2019),
(22, 'Avengers: Endgame', 2019),
(23, 'Spider-Man: Far From Home', 2019),
(24, 'Black Widow', 2021),
(25, 'Shang-Chi and the Legend of the Ten Rings', 2021),
(26, 'Eternals', 2021),
(27, 'Spider-Man: No Way Home', 2021),
(28, 'Doctor Strange in the Multiverse of Madness', 2022),
(29, 'Thor: Love and Thunder', 2022),
(30, 'Black Panther: Wakanda Forever', 2022);

-- Заполнение таблицы "fullname"
INSERT INTO marvel_movies_database.fullname (person_id, fullname) VALUES
-- Актеры
(2001, 'Robert Downey Jr.'), (2002, 'Gwyneth Paltrow'), (2003, 'Terrence Howard'),
(2004, 'Edward Norton'), (2005, 'Scarlett Johansson'), (2006, 'Chris Hemsworth'),
(2007, 'Tom Hiddleston'), (2008, 'Natalie Portman'), (2009, 'Chris Evans'),
(2010, 'Sebastian Stan'), (2011, 'Hayley Atwell'), (2012, 'Anthony Mackie'),
(2013, 'Chris Pratt'), (2014, 'Zoe Saldana'), (2015, 'Dave Bautista'),
(2016, 'Paul Rudd'), (2017, 'Michael Douglas'), (2018, 'Evangeline Lilly'),
(2019, 'Benedict Cumberbatch'), (2020, 'Tilda Swinton'), (2021, 'Chiwetel Ejiofor'),
(2022, 'Tom Holland'), (2023, 'Michael Keaton'), (2024, 'Cate Blanchett'),
(2025, 'Chadwick Boseman'), (2026, 'Lupita Nyongo'), (2027, 'Michael B. Jordan'),
(2028, 'Brie Larson'), (2029, 'Jude Law'), (2030, 'Samuel L. Jackson'),
(2031, 'Jake Gyllenhaal'),
(2032, 'Florence Pugh'), (2033, 'David Harbour'),
(2034, 'Simu Liu'), (2035, 'Tony Leung'), (2036, 'Meng er Zhang'),
(2037, 'Gemma Chan'), (2038, 'Richard Madden'), (2039, 'Salma Hayek'),
(2040, 'Jamie Foxx'),
(2041, 'Elizabeth Olsen'), (2042, 'Xochitl Gomez'),
(2043, 'Letitia Wright'), (2044, 'Tenoch Huerta'),

-- Съемочная группа
-- Iron Man (2008)
(1001, 'Jon Favreau'), (1002, 'Matthew Libatique'),  (1004, 'Kevin Feige'), (1005, 'Mark Fergus'),
-- The Incredible Hulk (2008)
(1006, 'Louis Leterrier'), (1007, 'Peter Menzies Jr.'), (1008, 'Craig Armstrong'), (1009, 'Avi Arad'), (1010, 'Zak Penn'),
-- Iron Man 2 (2010)
  (1013, 'John Debney'),  (1015, 'Justin Theroux'),
-- Thor (2011)
(1016, 'Kenneth Branagh'), (1017, 'Haris Zambarloukos'), (1018, 'Patrick Doyle'),  (1020, 'Ashley Miller'),
-- Captain America: The First Avenger (2011)
(1021, 'Joe Johnston'), (1022, 'Shelly Johnson'), (1023, 'Alan Silvestri'),  (1025, 'Christopher Markus'),
-- The Avengers (2012)
(1026, 'Joss Whedon'), (1027, 'Seamus McGarvey'), (1030, 'Kai Cole'),
-- Iron Man 3 (2013)
(1031, 'Shane Black'), (1032, 'John Toll'), (1033, 'Brian Tyler'),  (1035, 'Drew Pearce'),
-- Thor: The Dark World (2013)
(1036, 'Alan Taylor'), (1037, 'Kramer Morgenthau'), (1038, 'Brian Tyler'),  (1040, 'Christopher Yost'),
-- Captain America: The Winter Soldier (2014)
(1041, 'Anthony Russo'), (1042, 'Trent Opaloch'), (1043, 'Henry Jackman'),  (1045, 'Christopher Markus'),
-- Guardians of the Galaxy (2014)
(1046, 'James Gunn'), (1047, 'Ben Davis'), (1048, 'Tyler Bates'),  (1049, 'Nicole Perlman'),
-- Avengers: Age of Ultron (2015)
 (1053, 'Brian Tyler'),  
-- Ant-Man (2015)
(1056, 'Peyton Reed'), (1057, 'Russell Carpenter'), (1058, 'Christophe Beck'),  (1060, 'Edgar Wright'),
-- Captain America: Civil War (2016)
(1061, 'Anthony Russo'), (1062, 'Trent Opaloch'), (1063, 'Henry Jackman'),  (1065, 'Christopher Markus'),
-- Doctor Strange (2016)
(1066, 'Scott Derrickson'), (1070, 'Jon Spaihts'),
-- Guardians of the Galaxy Vol. 2 (2017)
 (1072, 'Henry Braham'), (1073, 'Tyler Bates'),  
-- Spider-Man: Homecoming (2017)
(1076, 'Jon Watts'), (1077, 'Salvatore Totino'),   (1080, 'John Francis Daley'),
-- Thor: Ragnarok (2017)
(1081, 'Taika Waititi'), (1082, 'Javier Aguirresarobe'), (1083, 'Mark Mothersbaugh'),  (1085, 'Eric Pearson'),
-- Black Panther (2018)
(1086, 'Ryan Coogler'), (1087, 'Rachel Morrison'), (1088, 'Ludwig Göransson'),  (1090, 'Joe Robert Cole'),
-- Avengers: Infinity War (2018)
(1091, 'Anthony Russo'), (1092, 'Trent Opaloch'), (1093, 'Alan Silvestri'),  (1095, 'Christopher Markus'),
-- Ant-Man and the Wasp (2018)
(1096, 'Peyton Reed'), (1097, 'Dante Spinotti'), (1098, 'Christophe Beck'),  (1100, 'Chris McKenna'),
-- Captain Marvel (2019)
(1101, 'Anna Boden'),  (1103, 'Pinar Toprak'),  (1105, 'Geneva Robertson-Dworet'),
-- Avengers: Endgame (2019)
(1107, 'Trent Opaloch'), (1108, 'Alan Silvestri'),  (1110, 'Christopher Markus'),
-- Spider-Man: Far From Home (2019)
 (1112, 'Matthew J. Lloyd'), (1068, 'Michael Giacchino'),  (1115, 'Chris McKenna'),
-- Black Widow (2021)
(1116, 'Cate Shortland'), (1117, 'Gabriel Beristain'), (1118, 'Lorne Balfe'),  (1120, 'Eric Pearson'),
-- Shang-Chi and the Legend of the Ten Rings (2021)
(1121, 'Destin Daniel Cretton'), (1122, 'Bill Pope'), (1123, 'Joel P. West'),  (1125, 'Dave Callaham'),
-- Eternals (2021)
(1126, 'Chloé Zhao'),  (1003, 'Ramin Djawadi'),  (1130, 'Chloé Zhao'),
-- Spider-Man: No Way Home (2021)
 (1132, 'Mauro Fiore'),   (1135, 'Chris McKenna'),
-- Doctor Strange in the Multiverse of Madness (2022)
(1136, 'Sam Raimi'), (1137, 'John Mathieson'), (1138, 'Danny Elfman'),  (1140, 'Michael Waldron'),
-- Thor: Love and Thunder (2022)
 (1142, 'Barry Idoine'),   (1145, 'Jennifer Kaytin Robinson'),
-- Black Panther: Wakanda Forever (2022)
(1146, 'Ryan Coogler'), (1147, 'Autumn Durald Arkapaw'), (1148, 'Ludwig Göransson'),  (1150, 'Joe Robert Cole');

-- Заполнение таблицы "crew"
INSERT INTO marvel_movies_database.crew (crew_worker_id, movie_id) VALUES
-- Iron Man (2008)
(1001, 1), (1002, 1), (1003, 1), (1004, 1), (1005, 1),
-- The Incredible Hulk (2008)
(1006, 2), (1007, 2), (1008, 2), (1009, 2), (1010, 2),
-- Iron Man 2 (2010)
(1001, 3), (1002, 3), (1013, 3), (1004, 3), (1015, 3),
-- Thor (2011)
(1016, 4), (1017, 4), (1018, 4), (1004, 4), (1020, 4),
-- Captain America: The First Avenger (2011)
(1021, 5), (1022, 5), (1023, 5), (1004, 5), (1025, 5),
-- The Avengers (2012)
(1026, 6), (1027, 6), (1023, 6), (1004, 6), (1030, 6),
-- Iron Man 3 (2013)
(1031, 7), (1032, 7), (1033, 7), (1004, 7), (1035, 7),
-- Thor: The Dark World (2013)
(1036, 8), (1037, 8), (1038, 8), (1004, 8), (1040, 8),
-- Captain America: The Winter Soldier (2014)
(1041, 9), (1042, 9), (1043, 9), (1004, 9), (1045, 9),
-- Guardians of the Galaxy (2014)
(1046, 10), (1047, 10), (1048, 10), (1004, 10), (1049, 10),
-- Avengers: Age of Ultron (2015)
(1026, 11), (1047, 11), (1053, 11), (1004, 11), (1030, 11),
-- Ant-Man (2015)
(1056, 12), (1057, 12), (1058, 12), (1004, 12), (1060, 12),
-- Captain America: Civil War (2016)
(1041, 13), (1062, 13), (1063, 13), (1004, 13), (1065, 13),
-- Doctor Strange (2016)
(1066, 14), (1047, 14), (1068, 14), (1004, 14), (1070, 14),
-- Guardians of the Galaxy Vol. 2 (2017)
(1046, 15), (1072, 15), (1073, 15), (1004, 15), (1049, 15),
-- Spider-Man: Homecoming (2017)
(1076, 16), (1077, 16), (1068, 16), (1004, 16), (1080, 16),
-- Thor: Ragnarok (2017)
(1081, 17), (1082, 17), (1083, 17), (1004, 17), (1085, 17),
-- Black Panther (2018)
(1086, 18), (1087, 18), (1088, 18), (1004, 18), (1090, 18),
-- Avengers: Infinity War (2018)
(1041, 19), (1092, 19), (1093, 19), (1004, 19), (1095, 19),
-- Ant-Man and the Wasp (2018)
(1096, 20), (1097, 20), (1098, 20), (1004, 20), (1100, 20),
-- Captain Marvel (2019)
(1101, 21), (1047, 21), (1103, 21), (1004, 21), (1105, 21),
-- Avengers: Endgame (2019)
(1041, 22), (1107, 22), (1108, 22), (1004, 22), (1110, 22),
-- Spider-Man: Far From Home (2019)
(1076, 23), (1112, 23), (1068, 23), (1004, 23), (1115, 23),
-- Black Widow (2021)
(1116, 24), (1117, 24), (1118, 24), (1004, 24), (1120, 24),
-- Shang-Chi and the Legend of the Ten Rings (2021)
(1121, 25), (1122, 25), (1123, 25), (1004, 25), (1125, 25),
-- Eternals (2021)
(1126, 26), (1047, 26), (1003, 26), (1004, 26), (1130, 26),
-- Spider-Man: No Way Home (2021)
(1076, 27), (1132, 27), (1068, 27), (1004, 27), (1135, 27),
-- Doctor Strange in the Multiverse of Madness (2022)
(1136, 28), (1137, 28), (1138, 28), (1004, 28), (1140, 28),
-- Thor: Love and Thunder (2022)
(1081, 29), (1142, 29), (1068, 29), (1004, 29), (1045, 29),
-- Black Panther: Wakanda Forever (2022)
(1146, 30), (1147, 30), (1148, 30), (1004, 30), (1150, 30);
-- Заполнение таблицы "roles"
INSERT INTO marvel_movies_database.roles (actor_id, movie_id, role) VALUES
-- Iron Man (2008)
(2001, 1, 'Tony Stark / Iron Man'), (2002, 1, 'Pepper Potts'), (2003, 1, 'James Rhodes'),
-- The Incredible Hulk (2008)
(2004, 2, 'Bruce Banner'), 
-- Iron Man 2 (2010)
(2001, 3, 'Tony Stark / Iron Man'), (2002, 3, 'Pepper Potts'), (2005, 3, 'Natasha Romanoff / Black Widow'),
-- Thor (2011)
(2006, 4, 'Thor'), (2007, 4, 'Loki'), (2008, 4, 'Jane Foster'),
-- Captain America: The First Avenger (2011)
(2009, 5, 'Steve Rogers / Captain America'), (2010, 5, 'James Barnes'), (2011, 5, 'Peggy Carter'),
-- The Avengers (2012)
(2001, 6, 'Tony Stark / Iron Man'), (2006, 6, 'Thor'), (2005, 6, 'Natasha Romanoff / Black Widow'),
-- Iron Man 3 (2013)
(2001, 7, 'Tony Stark / Iron Man'), (2002, 7, 'Pepper Potts'), 
-- Thor: The Dark World (2013)
(2006, 8, 'Thor'), (2007, 8, 'Loki'), (2008, 8, 'Jane Foster'),
-- Captain America: The Winter Soldier (2014)
(2009, 9, 'Steve Rogers / Captain America'), (2005, 9, 'Natasha Romanoff / Black Widow'), (2012, 9, 'Sam Wilson'),
-- Guardians of the Galaxy (2014)
(2013, 10, 'Peter Quill / Star-Lord'), (2014, 10, 'Gamora'), (2015, 10, 'Drax'),
-- Avengers: Age of Ultron (2015)
(2001, 11, 'Tony Stark / Iron Man'), (2006, 11, 'Thor'), (2005, 11, 'Natasha Romanoff / Black Widow'),
-- Ant-Man (2015)
(2016, 12, 'Scott Lang / Ant-Man'), (2017, 12, 'Hank Pym'), (2018, 12, 'Hope van Dyne'),
-- Captain America: Civil War
(2009, 13, 'Steve Rogers / Captain America'), (2005, 13, 'Natasha Romanoff / Black Widow'), (2027, 13, 'Sam Wilson'),
-- Doctor Strange
(2019, 14, 'Doctor Stephen Strange'), (2020, 14, 'The Ancient One'), (2021, 14, 'Karl Mordo'),
-- Guardians of the Galaxy Vol. 2
(2013, 15, 'Peter Quill / Star-Lord'), (2014, 15, 'Gamora'), (2015, 15, 'Drax'),
-- Spider-Man: Homecoming
(2022, 16, 'Peter Parker / Spider-Man'), (2001, 16, 'Tony Stark / Iron Man'), (2023, 16, 'Vulture'),
-- Thor: Ragnarok
(2006, 17, 'Thor'), (2007, 17, 'Loki'), (2024, 17, 'Hela'),
-- Black Panther
(2025, 18, 'TChalla / Black Panther'), (2026, 18, 'Nakia'), (2027, 18, 'Killmonger'),
-- Avengers: Infinity War
(2001, 19, 'Tony Stark / Iron Man'), (2006, 19, 'Thor'), (2005, 19, 'Natasha Romanoff / Black Widow'),
-- Ant-Man and the Wasp
(2016, 20, 'Scott Lang / Ant-Man'), (2018, 20, 'Hope van Dyne / Wasp'), (2017, 20, 'Hank Pym'),
-- Captain Marvel
(2028, 21, 'Carol Danvers / Captain Marvel'), (2030, 21, 'Nick Fury'), (2029, 21, 'Yon-Rogg'),
-- Avengers: Endgame
(2001, 22, 'Tony Stark / Iron Man'), (2009, 22, 'Steve Rogers / Captain America'), (2006, 22, 'Thor'),
-- Spider-Man: Far From Home
(2022, 23, 'Peter Parker / Spider-Man'), (2031, 23, 'Mysterio'), (2030, 23, 'Nick Fury'),
-- Black Widow
(2002, 24, 'Natasha Romanoff / Black Widow'), (2032, 24, 'Yelena Belova'), (2033, 24, 'Alexei Shostakov'),
-- Shang-Chi and the Legend of the Ten Rings
(2034, 25, 'Shang-Chi'), (2035, 25, 'Wenwu'), (2036, 25, 'Xu Xialing'),
-- Eternals
(2037, 26, 'Sersi'), (2038, 26, 'Ikaris'), (2039, 26, 'Ajak'),
-- Spider-Man: No Way Home
(2022, 27, 'Peter Parker / Spider-Man'), (2019, 27, 'Doctor Strange'), (2040, 27, 'Electro'),
-- Doctor Strange in the Multiverse of Madness
(2019, 28, 'Doctor Stephen Strange'), (2041, 28, 'Wanda Maximoff'), (2042, 28, 'America Chavez'),
-- Thor: Love and Thunder
(2006, 29, 'Thor'), (2008, 29, 'Jane Foster'), (2007, 29, 'Loki'),
-- Black Panther: Wakanda Forever
(2025, 30, 'TChalla / Black Panther'), (2043, 30, 'Shuri'), (2044, 30, 'Namor');

-- Заполнение таблицы "crew_x_person"
INSERT INTO marvel_movies_database.crew_x_person (crew_worker_id, movie_id, post) VALUES
-- Iron Man (2008)
(1001, 1, 'Director'), (1002, 1, 'Cinematographer'), (1003, 1, 'Composer'), (1004, 1, 'Producer'), (1005, 1, 'Screenwriter'),
-- The Incredible Hulk (2008)
(1006, 2, 'Director'), (1007, 2, 'Cinematographer'), (1008, 2, 'Composer'), (1009, 2, 'Producer'), (1010, 2, 'Screenwriter'),
-- Iron Man 2 (2010)
(1001, 3, 'Director'), (1002, 3, 'Cinematographer'), (1013, 3, 'Composer'), (1004, 3, 'Producer'), (1015, 3, 'Screenwriter'),
-- Thor (2011)
(1016, 4, 'Director'), (1017, 4, 'Cinematographer'), (1018, 4, 'Composer'), (1004, 4, 'Producer'), (1020, 4, 'Screenwriter'),
-- Captain America: The First Avenger (2011)
(1021, 5, 'Director'), (1022, 5, 'Cinematographer'), (1023, 5, 'Composer'), (1004, 5, 'Producer'), (1025, 5, 'Screenwriter'),
-- The Avengers (2012)
(1026, 6, 'Director'), (1027, 6, 'Cinematographer'), (1023, 6, 'Composer'), (1004, 6, 'Producer'), (1030, 6, 'Screenwriter'),
-- Iron Man 3 (2013)
(1031, 7, 'Director'), (1032, 7, 'Cinematographer'), (1033, 7, 'Composer'), (1004, 7, 'Producer'), (1035, 7, 'Screenwriter'),
-- Thor: The Dark World (2013)
(1036, 8, 'Director'), (1037, 8, 'Cinematographer'), (1038, 8, 'Composer'), (1004, 8, 'Producer'), (1040, 8, 'Screenwriter'),
-- Captain America: The Winter Soldier (2014)
(1041, 9, 'Director'), (1042, 9, 'Cinematographer'), (1043, 9, 'Composer'), (1004, 9, 'Producer'), (1045, 9, 'Screenwriter'),
-- Guardians of the Galaxy (2014)
(1046, 10, 'Director'), (1047, 10, 'Cinematographer'), (1048, 10, 'Composer'), (1004, 10, 'Producer'), (1049, 10, 'Screenwriter'),
-- Avengers: Age of Ultron (2015)
(1026, 11, 'Director'), (1047, 11, 'Cinematographer'), (1053, 11, 'Composer'), (1004, 11, 'Producer'), (1030, 11, 'Screenwriter'),
-- Ant-Man (2015)
(1056, 12, 'Director'), (1057, 12, 'Cinematographer'), (1058, 12, 'Composer'), (1004, 12, 'Producer'), (1060, 12, 'Screenwriter'),
-- Captain America: Civil War (2016)
(1041, 13, 'Director'), (1062, 13, 'Cinematographer'), (1063, 13, 'Composer'), (1004, 13, 'Producer'), (1065, 13, 'Screenwriter'),
-- Doctor Strange (2016)
(1066, 14, 'Director'), (1047, 14, 'Cinematographer'), (1068, 14, 'Composer'), (1004, 14, 'Producer'), (1070, 14, 'Screenwriter'),
-- Guardians of the Galaxy Vol. 2 (2017)
(1046, 15, 'Director'), (1072, 15, 'Cinematographer'), (1073, 15, 'Composer'), (1004, 15, 'Producer'), (1049, 15, 'Screenwriter'),
-- Spider-Man: Homecoming (2017)
(1076, 16, 'Director'), (1077, 16, 'Cinematographer'), (1068, 16, 'Composer'), (1004, 16, 'Producer'), (1080, 16, 'Screenwriter'),
-- Thor: Ragnarok (2017)
(1081, 17, 'Director'), (1082, 17, 'Cinematographer'), (1083, 17, 'Composer'), (1004, 17, 'Producer'), (1085, 17, 'Screenwriter'),
-- Black Panther (2018)
(1086, 18, 'Director'), (1087, 18, 'Cinematographer'), (1088, 18, 'Composer'), (1004, 18, 'Producer'), (1090, 18, 'Screenwriter'),
-- Avengers: Infinity War (2018)
(1041, 19, 'Director'), (1092, 19, 'Cinematographer'), (1093, 19, 'Composer'), (1004, 19, 'Producer'), (1095, 19, 'Screenwriter'),
-- Ant-Man and the Wasp (2018)
(1096, 20, 'Director'), (1097, 20, 'Cinematographer'), (1098, 20, 'Composer'), (1004, 20, 'Producer'), (1100, 20, 'Screenwriter'),
-- Captain Marvel (2019)
(1101, 21, 'Director'), (1047, 21, 'Cinematographer'), (1103, 21, 'Composer'), (1004, 21, 'Producer'), (1105, 21, 'Screenwriter'),
-- Avengers: Endgame (2019)
(1041, 22, 'Director'), (1107, 22, 'Cinematographer'), (1108, 22, 'Composer'), (1004, 22, 'Producer'), (1110, 22, 'Screenwriter'),
-- Spider-Man: Far From Home (2019)
(1076, 23, 'Director'), (1112, 23, 'Cinematographer'), (1068, 23, 'Composer'), (1004, 23, 'Producer'), (1115, 23, 'Screenwriter'),
-- Black Widow (2021)
(1116, 24, 'Director'), (1117, 24, 'Cinematographer'), (1118, 24, 'Composer'), (1004, 24, 'Producer'), (1120, 24, 'Screenwriter'),
-- Shang-Chi and the Legend of the Ten Rings (2021)
(1121, 25, 'Director'), (1122, 25, 'Cinematographer'), (1123, 25, 'Composer'), (1004, 25, 'Producer'), (1125, 25, 'Screenwriter'),
-- Eternals (2021)
(1126, 26, 'Director'), (1047, 26, 'Cinematographer'), (1003, 26, 'Composer'), (1004, 26, 'Producer'), (1130, 26, 'Screenwriter'),
-- Spider-Man: No Way Home (2021)
(1076, 27, 'Director'), (1132, 27, 'Cinematographer'), (1068, 27, 'Composer'), (1004, 27, 'Producer'), (1135, 27, 'Screenwriter'),
-- Doctor Strange in the Multiverse of Madness (2022)
(1136, 28, 'Director'), (1137, 28, 'Cinematographer'), (1138, 28, 'Composer'), (1004, 28, 'Producer'), (1140, 28, 'Screenwriter'),
-- Thor: Love and Thunder (2022)
(1081, 29, 'Director'), (1142, 29, 'Cinematographer'), (1068, 29, 'Composer'), (1004, 29, 'Producer'), (1045, 29, 'Screenwriter'),
-- Black Panther: Wakanda Forever (2022)
(1146, 30, 'Director'), (1147, 30, 'Cinematographer'), (1148, 30, 'Composer'), (1004, 30, 'Producer'), (1150, 30, 'Screenwriter');

-- Заполнение таблицы "salary"
INSERT INTO marvel_movies_database.salary (worker_id, movie_id, salary) VALUES
-- Iron Man (2008)
(2001, 1, 8000000), (2002, 1, 2000000), (2003, 1, 1000000),
-- The Incredible Hulk (2008)
(2004, 2, 4000000),
-- Iron Man 2 (2010)
(2001, 3, 10000000), (2002, 3, 3000000), (2005, 3, 3000000),
-- Thor (2011)
(2006, 4, 2000000), (2007, 4, 1000000), (2008, 4, 800000),
-- Captain America: The First Avenger (2011)
(2009, 5, 3000000), (2010, 5, 500000), (2011, 5, 600000),
-- The Avengers (2012)
(2001, 6, 15000000), (2006, 6, 5000000), (2005, 6, 4000000),
-- Iron Man 3 (2013)
(2001, 7, 12000000), (2002, 7, 3000000),
-- Thor: The Dark World (2013)
(2006, 8, 4000000), (2007, 8, 1500000), (2008, 8, 800000),
-- Captain America: The Winter Soldier (2014)
(2009, 9, 4000000), (2005, 9, 3000000), (2012, 9, 800000),
-- Guardians of the Galaxy (2014)
(2013, 10, 3000000), (2014, 10, 2000000), (2015, 10, 1500000),
-- Avengers: Age of Ultron (2015)
(2001, 11, 20000000), (2006, 11, 8000000), (2005, 11, 6000000),
-- Ant-Man (2015)
(2016, 12, 1500000), (2017, 12, 1000000), (2018, 12, 1000000),
-- Captain America: Civil War (2016)
(2009, 13, 5000000), (2005, 13, 4000000), (2027, 13, 2000000),
-- Doctor Strange (2016)
(2019, 14, 3000000), (2020, 14, 2000000), (2021, 14, 1500000),
-- Guardians of the Galaxy Vol. 2 (2017)
(2013, 15, 5000000), (2014, 15, 2000000), (2015, 15, 1500000),
-- Spider-Man: Homecoming (2017)
(2022, 16, 2000000), (2001, 16, 1500000), (2023, 16, 1000000),
-- Thor: Ragnarok (2017)
(2006, 17, 8000000), (2007, 17, 2000000), (2024, 17, 3000000),
-- Black Panther (2018)
(2025, 18, 2000000), (2026, 18, 1500000), (2027, 18, 2000000),
-- Avengers: Infinity War (2018)
(2001, 19, 25000000), (2006, 19, 10000000), (2005, 19, 7000000),
-- Ant-Man and the Wasp (2018)
(2016, 20, 3000000), (2018, 20, 2000000), (2017, 20, 1500000),
-- Captain Marvel (2019)
(2028, 21, 5000000), (2030, 21, 4000000), (2029, 21, 3000000),
-- Avengers: Endgame (2019)
(2001, 22, 30000000), (2009, 22, 10000000), (2006, 22, 10000000),
-- Spider-Man: Far From Home (2019)
(2022, 23, 3000000), (2031, 23, 3000000), (2030, 23, 4000000),
-- Black Widow (2021)
(2002, 24, 15000000), (2032, 24, 2000000), (2033, 24, 1500000),
-- Shang-Chi and the Legend of the Ten Rings (2021)
(2034, 25, 2000000), (2035, 25, 1500000), (2036, 25, 1000000),
-- Eternals (2021)
(2037, 26, 2000000), (2038, 26, 2000000), (2039, 26, 1500000),
-- Spider-Man: No Way Home (2021)
(2022, 27, 4000000), (2019, 27, 5000000), (2040, 27, 3000000),
-- Doctor Strange in the Multiverse of Madness (2022)
(2019, 28, 6000000), (2041, 28, 4000000), (2042, 28, 2000000),
-- Thor: Love and Thunder (2022)
(2006, 29, 12000000), (2008, 29, 2000000), (2007, 29, 3000000),
-- Black Panther: Wakanda Forever (2022)
(2025, 30, 4000000), (2043, 30, 3000000), (2044, 30, 2000000);
