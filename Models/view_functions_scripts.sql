CREATE VIEW marvel_movies_database.ViewActorMovieInfo AS
SELECT r.movie_id, m.movie_name, m.year, r.role, f.fullname
FROM marvel_movies_database.roles r
JOIN marvel_movies_database.movies m ON r.movie_id = m.movie_id
JOIN marvel_movies_database.fullname f ON r.actor_id = f.person_id;

CREATE OR REPLACE VIEW marvel_movies_database.ViewCrewAndActorSalaries AS
SELECT DISTINCT ON (c.crew_worker_id, c.movie_id, cxp.post)
    c.crew_worker_id, 
    f.fullname, 
    c.movie_id, 
    m.movie_name, 
    s.salary, 
    cxp.post  
FROM marvel_movies_database.crew c
JOIN marvel_movies_database.fullname f ON c.crew_worker_id = f.person_id
JOIN marvel_movies_database.movies m ON c.movie_id = m.movie_id
JOIN marvel_movies_database.salary s ON c.crew_worker_id = s.worker_id
JOIN marvel_movies_database.crew_x_person cxp ON c.crew_worker_id = cxp.crew_worker_id
ORDER BY c.crew_worker_id, c.movie_id, cxp.post, s.salary DESC;

CREATE OR REPLACE VIEW marvel_movies_database.ViewFullCrewDetails AS
SELECT 
    DISTINCT ON (c.crew_worker_id, c.movie_id, cxp.post)
    c.crew_worker_id AS worker_id, 
    f.fullname, 
    cxp.post, 
    m.movie_name, 
    m.year
FROM 
    marvel_movies_database.crew c
JOIN 
    marvel_movies_database.fullname f ON c.crew_worker_id = f.person_id
JOIN 
    marvel_movies_database.movies m ON c.movie_id = m.movie_id
JOIN 
    marvel_movies_database.crew_x_person cxp ON c.crew_worker_id = cxp.crew_worker_id
ORDER BY 
    c.crew_worker_id, c.movie_id, cxp.post;

CREATE VIEW marvel_movies_database.ViewMovieYearRole AS
SELECT m.year, m.movie_name, m.movie_id, STRING_AGG(f.fullname, ', ') AS actors
FROM marvel_movies_database.movies m
JOIN marvel_movies_database.roles r ON m.movie_id = r.movie_id
JOIN marvel_movies_database.fullname f ON r.actor_id = f.person_id
GROUP BY m.year, m.movie_name, m.movie_id
ORDER BY m.year, m.movie_id ASC;

CREATE OR REPLACE FUNCTION marvel_movies_database.get_movies_by_actor(actor_name VARCHAR)
RETURNS TABLE(movie_name VARCHAR, year INT) AS $$
BEGIN
    RETURN QUERY
    SELECT m.movie_name, m.year
    FROM marvel_movies_database.movies m
    JOIN marvel_movies_database.roles r ON m.movie_id = r.movie_id
    JOIN marvel_movies_database.fullname f ON r.actor_id = f.person_id
    WHERE f.fullname = actor_name;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION marvel_movies_database.count_movies_by_year(release_year INT)
RETURNS INTEGER AS $$
DECLARE
    movie_count INT;
BEGIN
    SELECT COUNT(*) INTO movie_count
    FROM marvel_movies_database.movies
    WHERE year = release_year;
    RETURN movie_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE marvel_movies_database.add_new_actor(full_name VARCHAR)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO marvel_movies_database.fullname (fullname)
    VALUES (full_name);
END;
$$;

CREATE OR REPLACE PROCEDURE marvel_movies_database.update_movie_year(movie_name VARCHAR, new_year INT)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE marvel_movies_database.movies
    SET year = new_year
    WHERE movie_name = movie_name;
END;
$$;