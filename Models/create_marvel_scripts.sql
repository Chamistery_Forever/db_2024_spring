CREATE SCHEMA marvel_movies_database;

-- Информация о фильмах
CREATE TABLE marvel_movies_database.movies (
    movie_id INTEGER PRIMARY KEY,
    movie_name VARCHAR(255) NOT NULL UNIQUE,
    year INTEGER NOT NULL
);

-- Информация о ФИО
CREATE TABLE marvel_movies_database.fullname (
    person_id INTEGER PRIMARY KEY,
    fullname VARCHAR(255) NOT NULL
);

-- Информация о главных ролях
CREATE TABLE marvel_movies_database.roles (
    actor_id INTEGER,
    role VARCHAR(255) NOT NULL,
    movie_id INTEGER NOT NULL,
    FOREIGN KEY (movie_id)
    REFERENCES marvel_movies_database.movies (movie_id),
    FOREIGN KEY (actor_id)
    REFERENCES marvel_movies_database.fullname (person_id),
    PRIMARY KEY (actor_id, movie_id)
);

-- Информация о съемочной группе
CREATE TABLE marvel_movies_database.crew (
    crew_worker_id INTEGER NOT NULL,
    movie_id INTEGER NOT NULL,
    FOREIGN KEY (movie_id)
    REFERENCES marvel_movies_database.movies (movie_id),
    FOREIGN KEY (crew_worker_id)
    REFERENCES marvel_movies_database.fullname (person_id),
    PRIMARY KEY (crew_worker_id, movie_id)
);

-- Сопоставление работника и должности
CREATE TABLE marvel_movies_database.crew_x_person (
    crew_worker_id INTEGER NOT NULL,
    movie_id INTEGER NOT NULL,
    post VARCHAR(255) NOT NULL,
    FOREIGN KEY (crew_worker_id, movie_id)
    REFERENCES marvel_movies_database.crew (crew_worker_id, movie_id)
);

-- Гонорар
CREATE TABLE marvel_movies_database.salary (
    worker_id INTEGER NOT NULL,
    movie_id INTEGER NOT NULL,
    salary INTEGER NOT NULL,
    FOREIGN KEY (worker_id, movie_id)
    REFERENCES marvel_movies_database.roles (actor_id, movie_id)
);
