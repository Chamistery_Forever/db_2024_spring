UPDATE marvel_movies_database.salary
SET salary = 10000000
WHERE worker_id = 2001 AND movie_id = 1;

UPDATE marvel_movies_database.roles
SET role = 'Thor Odinson'
WHERE actor_id = 2006 AND movie_id = 4;

DELETE FROM marvel_movies_database.salary
WHERE movie_id = 2;

UPDATE marvel_movies_database.movies
SET movie_name = 'Iron Man Not First'
WHERE movie_id = 3;

DELETE FROM marvel_movies_database.roles
WHERE actor_id = 2007 AND movie_id = 8;

SELECT movie_name, year
FROM marvel_movies_database.movies
WHERE year = 2018;

SELECT year, COUNT(*) AS count_movies
FROM marvel_movies_database.movies
GROUP BY year;

SELECT actor_id, COUNT(*) AS movies_count
FROM marvel_movies_database.roles
GROUP BY actor_id
HAVING movies_count > 3;

SELECT movie_id, SUM(salary) AS total_salaries
FROM marvel_movies_database.salary
WHERE salary > 1000000
GROUP BY movie_id;

SELECT movie_id, AVG(salary) AS average_salary
FROM marvel_movies_database.salary
GROUP BY movie_id
HAVING AVG(salary) > 5000000;

SELECT fullname, movie_name, salary,
       LAG(salary, 1) OVER (PARTITION BY worker_id ORDER BY movie_id) AS previous_salary
FROM marvel_movies_database.salary
JOIN marvel_movies_database.movies ON marvel_movies_database.salary.movie_id = marvel_movies_database.movies.movie_id
JOIN marvel_movies_database.fullname ON marvel_movies_database.salary.worker_id = marvel_movies_database.fullname.person_id;

SELECT year, movie_name, salary,
       RANK() OVER (PARTITION BY year ORDER BY salary DESC) AS rank_in_year
FROM marvel_movies_database.salary
JOIN marvel_movies_database.movies ON marvel_movies_database.salary.movie_id = marvel_movies_database.movies.movie_id;

SELECT movie_name, salary,
       PERCENT_RANK() OVER (ORDER BY salary) AS percent_rank
FROM marvel_movies_database.salary
JOIN marvel_movies_database.movies ON marvel_movies_database.salary.movie_id = marvel_movies_database.movies.movie_id;

SELECT movie_name, salary,
       CUME_DIST() OVER (ORDER BY salary) AS cumulative_distribution
FROM marvel_movies_database.salary
JOIN marvel_movies_database.movies ON marvel_movies_database.salary.movie_id = marvel_movies_database.movies.movie_id;

SELECT year, movie_name, salary,
       SUM(salary) OVER (ORDER BY year) AS running_total_salary
FROM marvel_movies_database.salary
JOIN marvel_movies_database.movies ON marvel_movies_database.salary.movie_id = marvel_movies_database.movies.movie_id;